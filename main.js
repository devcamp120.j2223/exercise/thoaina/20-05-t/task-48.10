"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gBASE_URL = "https://62454a477701ec8f724fb923.mockapi.io/api/v1/";
// Biến toàn cục để lưu trữ id grade đang đc update or delete. Mặc định = 0;
var gGradeId = 0;
var gListStudent = new Array();
var gListSubject = new Array();

// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gGRADE_COLS = ["stt", "studentId", "subjectId", "grade", "examDate", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gSTT_COL = 0;
const gSTUDENT_COL = 1;
const gSUBJECT_COL = 2;
const gGRADE_COL = 3;
const gDATE_COL = 4;
const gACTION_COL = 5;

// Biến toàn cục để hiển lưu STT
var gSTT = 1;
// Khai báo DataTable & mapping collumns
var gGradeTable = $("#grade-table").DataTable({
    columns: [
        { data: gGRADE_COLS[gSTT_COL] },
        { data: gGRADE_COLS[gSTUDENT_COL] },
        { data: gGRADE_COLS[gSUBJECT_COL] },
        { data: gGRADE_COLS[gGRADE_COL] },
        { data: gGRADE_COLS[gDATE_COL] },
        { data: gGRADE_COLS[gACTION_COL] }
    ],
    columnDefs: [
        { // định nghĩa lại cột STT
            targets: gSTT_COL,
            render: function () {
                return gSTT++;
            }
        },
        {
            targets: gSTUDENT_COL,
            render: getStudentByStudentId
        },
        {
            targets: gSUBJECT_COL,
            render: getSubjectBySubjectId
        },
        { // định nghĩa lại cột action
            targets: gACTION_COL,
            defaultContent: `
    <img class="edit-voucher" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
        <img class="delete-voucher" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
            `
        }
    ]
});

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$.ajax({
    url: gBASE_URL + "/students",
    type: "GET",
    dataType: "json",
    success: function (paramRes) {
        gListStudent = paramRes;
    },
    error: function (paramError) {
        console.assert(paramError.responseText);
    }
});
$.ajax({
    url: gBASE_URL + "/subjects",
    type: "GET",
    dataType: "json",
    success: function (paramRes) {
        gListSubject = paramRes;
    },
    error: function (paramError) {
        console.assert(paramError.responseText);
    }
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm thực thi khi trang được load
function onPageLoading() {
    callAPIGetGrade();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function callAPIGetGrade() {
    $.ajax({
        url: gBASE_URL + "/grades",
        type: "GET",
        dataType: "json",
        success: function (paramRes) {
            loadDataToGradeTable(paramRes);
        },
        error: function (paramError) {
            console.assert(paramError.responseText);
        }
    });
}

function loadDataToGradeTable(paramGradeArr) {
    gSTT = 1;
    gGradeTable.clear();
    gGradeTable.rows.add(paramGradeArr);
    gGradeTable.draw();
}

function getStudentByStudentId(paramData) {
    var vIsFound = false;
    var vName = "";
    var vIndex = 0;
    while (vIsFound == false && vIndex < gListStudent.length) {
        if (gListStudent[vIndex].id == paramData) {
            vName = gListStudent[vIndex].firstname + " " + gListStudent[vIndex].lastname;
            vIsFound = true;
        }
        vIndex++;
    }
    return vName;
}

function getSubjectBySubjectId(paramData) {
    var vIsFound = false;
    var vSubject = "";
    var vIndex = 0;
    while (vIsFound == false && vIndex < gListSubject.length) {
        if (gListSubject[vIndex].id == paramData) {
            vSubject = gListSubject[vIndex].subjectName;
            vIsFound = true;
        }
        vIndex++;
    }
    return vSubject;
}

